# CHECK24 UI and Mobile Test Framework

The framework is a great way to test CHECK24 apps on the browser, mobile devices (android/iOS) through the UI. It uses Selenium Webdriver for UI testing and Appium for Mobile app testing.

## Getting Started

Follow the following instructions to deploy the framework on your machine and run tests

### Prerequisites

* Selenium
* TestNG
* Maven
* Appium 
* Eclipse

## Running the tests

Use the following steps to run the tests

	TestNG suites files is in "/resources/suites" folder. There are two files one for web and one for mobile.

	1. Check24AndroidTestSuite.xml [Mobile(Android)]
	2. Check24WebTestSuite.xml [Web]

	Execution:

	Right click on TestNG XML file --> Run As --> TestNG Suite

	Web: You don't need any webdriver just follow the above execution steps.

	Mobile: Please start appium first and then start the emulator or connect the device. After that update the device details in 'AppiumDriverFactory.java' file and just follow the above execution steps.


