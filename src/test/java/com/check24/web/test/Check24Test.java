package com.check24.web.test;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.check24.web.base.BaseWebTestSuite;
import com.check24.web.base.WebConstants;
import com.check24.web.pagehelpermethods.HomePage;
import com.check24.web.pagehelpermethods.KfzVersicherungsvergleichPage;

public class Check24Test extends BaseWebTestSuite {
	
	@Test
	public void check24_Task_1_Test() {
		HomePage homePage = getHomePage();
		Assert.assertTrue(homePage.isHomePageLoad());
		
		homePage.clickOnAkzeptieren();
		Assert.assertTrue(homePage.isBestehendesAutoSelectionDisplayed());
		
		// Click ‚Bestehendes Auto‘ and click ‚Tarif vergleichen‘.
		homePage.clickOnBestehendesAuto();
		homePage.clickOnTarifeVergleichen();
		KfzVersicherungsvergleichPage kfzVersicherungsvergleichPage = getKfzVersicherungsvergleichPage();
		Assert.assertTrue(kfzVersicherungsvergleichPage.isAendernButtonDisplayed());
		
		// Click ‚ändern‘ here and then click ‚Weiter’
		kfzVersicherungsvergleichPage.clickOnAendernButton();
		Assert.assertTrue(kfzVersicherungsvergleichPage.isWeiterButtonDisplayed());
		
		kfzVersicherungsvergleichPage.clickOnWeiterButton();
		Assert.assertTrue(kfzVersicherungsvergleichPage.isVersicherungUploadDisplayed());
		
		// Give some file here. You can attach here ‘Test Automation Task-Professional.pdf’ task file. Following screen will be displayed.
		kfzVersicherungsvergleichPage.uploadVersicherung(WebConstants.VERSICHERUNG_FILE_PATH);
		Assert.assertTrue(kfzVersicherungsvergleichPage.isHochladenAndAnalysierenButtonDisplayed());
		
		// Click ‘jetzt hochladen und analysieren’
		kfzVersicherungsvergleichPage.clickOnHochladenAndAnalysierenButton();
		Assert.assertTrue(kfzVersicherungsvergleichPage.isNeinMitVorhandenemDokumentWeiterButtonDisplayed());
		
		// Click ‚nein, mit Vorhanden Dokumente Weiter‘. Following screen will be displayed
		kfzVersicherungsvergleichPage.clickOnNeinMitVorhandenemDokumentWeiterButton();
		Assert.assertTrue(kfzVersicherungsvergleichPage.isOhneAnmeldungFortfahrenButtonDisplayed());
		
		// Click ‘ohne Anmeldung fortfahren’, following screen will be displayed
		kfzVersicherungsvergleichPage.clickOnOhneAnmeldungFortfahrenButton();
		Assert.assertTrue(kfzVersicherungsvergleichPage.isFahrzeugSectionDisplayed());
		Assert.assertTrue(kfzVersicherungsvergleichPage.isFahrzeugauswahlüberLabelDisplayed());
		Assert.assertTrue(kfzVersicherungsvergleichPage.isHerstellernummerLabelDisplayed());
		Assert.assertTrue(kfzVersicherungsvergleichPage.isTypschlüsselnummerLabelDisplayed());
		Assert.assertTrue(kfzVersicherungsvergleichPage.isVersicherungsbeginnLabelDisplayed());
	}
}
