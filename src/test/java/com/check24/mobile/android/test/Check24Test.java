package com.check24.mobile.android.test;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.check24.mobile.base.BaseMobileTestSuite;
import com.check24.mobile.base.MobileConstants;
import com.check24.mobile.pagehelpermethods.android.HomePage;
import com.check24.mobile.pagehelpermethods.android.KfzVersicherungPage;

public class Check24Test extends BaseMobileTestSuite {
	
	@Test
	public void check24_Task_2_Test() {
		// Open Check24 App
		HomePage homePage = app.getHomePage();
		Assert.assertTrue(homePage.isTermAndConditionDisplayed());
		
		homePage.acceptTermAndCondition();
		Assert.assertTrue(homePage.isHomePageLoad());
		
		// Type ‚KFZ-Versicherung‘ in the text box, click it
		homePage.searchText(MobileConstants.SEARCH_TEXT);
		KfzVersicherungPage kfzVersicherungPage = app.getKfzVersicherungPage();
		Assert.assertTrue(kfzVersicherungPage.isSchnellpreisDisplayed());
		
		//Click Schnellpreis
		kfzVersicherungPage.clickOnSchnellpreis();
		Assert.assertTrue(kfzVersicherungPage.isZipFieldDisplayed());
		
		// Enter PLZ, click Weiter
		kfzVersicherungPage.enterZipcode(MobileConstants.ZIPCODE);
		Assert.assertTrue(kfzVersicherungPage.isDobFieldsDisplayed());
		
		// Enter Date of birth, click Weiter
		kfzVersicherungPage.enterDOB(MobileConstants.DOB_DAY, MobileConstants.DOB_MONTH, MobileConstants.DOB_YEAR);
		Assert.assertTrue(kfzVersicherungPage.isDriveBarDisplayed());
		
		// Select km as 11,000 km/Jahr, click Weiter
		kfzVersicherungPage.setDriveBar(MobileConstants.DRIVE);
		Assert.assertTrue(kfzVersicherungPage.isTextDisplayed(MobileConstants.WHO_DRIVE_CAR_PAGE_TITLE));
		
		// Click Weiter „Wer fährt mit dem Auto?“
		kfzVersicherungPage.clickOnWeiterButton();
		Assert.assertTrue(kfzVersicherungPage.isTextDisplayed(MobileConstants.CAR_INSURED_PAGE_TITLE));
		
		// Click ‘Weiter’
		kfzVersicherungPage.clickOnWeiterButton();
		Assert.assertTrue(kfzVersicherungPage.isTextDisplayed(MobileConstants.PAY_SCHEDULE_PAGE_TITLE));
		
		// Select ‚Jährlich‘
		kfzVersicherungPage.clickViaText(MobileConstants.PAY_YEARLY);
		kfzVersicherungPage.clickOnWeiterButton();
		Assert.assertTrue(kfzVersicherungPage.isHsnDisplayed());
		Assert.assertTrue(kfzVersicherungPage.isTsnDisplayed());
		
		// Enter 0588 as ‚HSN‘ and ‚ACI‘ as TSN and Click ‘jetzt Preise annzeigen’
		kfzVersicherungPage.enterCarDetails(MobileConstants.HSN, MobileConstants.TSN);
		Assert.assertTrue(kfzVersicherungPage.isTextDisplayed(MobileConstants.PROTECTION_PAGE_TITLE));
		
		// Verify that this screen gets displayed with Haftpflicht, Haftpflicht + Teilkasko, Haftpflicht+Vollkasko price range.
		Assert.assertTrue(kfzVersicherungPage.isKsTitleDisplayed());
		Assert.assertTrue(kfzVersicherungPage.isTkTitleDisplayed());
		Assert.assertTrue(kfzVersicherungPage.isVkTitleDisplayed());
		Assert.assertTrue(kfzVersicherungPage.isKsPriceDisplayed());
		Assert.assertTrue(kfzVersicherungPage.isTkPriceDisplayed());
		Assert.assertTrue(kfzVersicherungPage.isVkPriceDisplayed());
	}
}
