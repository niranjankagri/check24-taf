package com.check24.utils;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Utility class
 */
public class Util {

	public static Logger logger = LogManager.getLogger(Util.class.getName());

	/**
	 * Variables used as input to sleep functions, OR for various UI functions
	 **/

	public static int S_WAIT = 3;
	public static int M_WAIT = 5;
	public static int L_WAIT = 10;

	public static int S_TIMEOUT = 5;
	public static int M_TIMEOUT = 10;
	public static int L_TIMEOUT = 20;
	public static int XL_TIMEOUT = 30;
	public static int XXL_TIMEOUT = 60;

	/**
	 * Sleep for the mentioned time in seconds
	 * 
	 * @param timeInSecond
	 * @param info         - any message that needs to be displayed while sleeping
	 */
	public static void sleepInSec(int timeInSecond, String info) {
		if (info != null) {
			logger.info("sleeping for: " + timeInSecond + ",info: " + info);
		}

		try {
			Thread.sleep(timeInSecond * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sleep for the mentioned time in seconds
	 * 
	 * @param timeInSecond
	 */
	public static void sleepInSec(int timeInSecond) {
		sleepInSec(timeInSecond, null);
	}

}
