package com.check24.utils;

import java.util.concurrent.ConcurrentHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;


public class TestListener implements ITestListener {

	public Logger logger = LogManager.getLogger();
    private static final String LOG_4J_Path = "/resources/config/log4j2.xml";
    private ConcurrentHashMap<String, ExtentTest> allTests = new ConcurrentHashMap<>();
    String reportFolderPath = System.getProperty("user.dir") + "//AutomationReports//";
    String reportName = "AutomationReport.html";

    public void onTestStart(ITestResult result) {
        logger.info("############## STARTING TEST " + result.getName() + " ####################");
        ExtentTest extentTest = ExtentManager.createInstance(reportFolderPath, reportName).createTest(result.getName());
        allTests.put(result.getName(), extentTest);
        ExtentManager.setTest(extentTest);
        ExtentManager.getInstance().flush();
    }
    
    public void onTestSuccess(ITestResult result) {
        logger.info("------------------- " + result.getName() + " : PASSED -------------------");
        ExtentManager.getTest().get().assignCategory(result.getName());
        ExtentManager.getTest().get().createNode(MarkupHelper.createLabel("Test passed", ExtentColor.GREEN).getMarkup());
        ExtentManager.getInstance().flush();
    }

    public void onTestFailure(ITestResult result) {
        logger.info("------------------- " + result.getName() + " : FAILED -------------------");
        try {
            ExtentManager.getTest().get().createNode(MarkupHelper.createLabel("Test Failed", ExtentColor.RED).getMarkup())
                    .fail(result.getThrowable());
            ExtentManager.getInstance().flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
       

    static {
        System.setProperty("log4j.configurationFile", System.getProperty("user.dir") + LOG_4J_Path);
    }

	@Override
	public void onTestSkipped(ITestResult result) {
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		
	}

	@Override
	public void onStart(ITestContext context) {
		
	}

	@Override
	public void onFinish(ITestContext context) {
		
	}
	
}
