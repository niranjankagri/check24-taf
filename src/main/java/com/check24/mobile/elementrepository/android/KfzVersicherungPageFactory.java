package com.check24.mobile.elementrepository.android;

import com.check24.mobile.base.BaseMobilePage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class KfzVersicherungPageFactory extends BaseMobilePage {

	public KfzVersicherungPageFactory(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
	
	@AndroidFindBy(xpath = "//android.widget.FrameLayout[@content-desc=\"39\"]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView")
	public static AndroidElement schnellpreis;
	
	@AndroidFindBy(id = "de.check24.check24:id/zip_input")
	public static AndroidElement zip;
	
	@AndroidFindBy(id = "de.check24.check24:id/hint")
	public static AndroidElement zipHint;
	
	@AndroidFindBy(id = "de.check24.check24:id/cta")
	public static AndroidElement weiter;
	
	@AndroidFindBy(id = "de.check24.check24:id/input_day")
	public static AndroidElement dobDay;
	
	@AndroidFindBy(id = "de.check24.check24:id/input_month")
	public static AndroidElement dobMonth;
	
	@AndroidFindBy(id = "de.check24.check24:id/input_year")
	public static AndroidElement dobYear;
	
	@AndroidFindBy(id = "de.check24.check24:id/seek_bar")
	public static AndroidElement driveBar;
	
	@AndroidFindBy(id = "de.check24.check24:id/value")
	public static AndroidElement driveValue;
	
	@AndroidFindBy(id = "de.check24.check24:id/title")
	public static AndroidElement pageTitle;
	
	@AndroidFindBy(id = "de.check24.check24:id/hsn")
	public static AndroidElement hsnField;
	
	@AndroidFindBy(id = "de.check24.check24:id/tsn")
	public static AndroidElement tsnField;
	
	@AndroidFindBy(id = "de.check24.check24:id/kh")
	public static AndroidElement ksTitle;
	
	@AndroidFindBy(id = "de.check24.check24:id/tk")
	public static AndroidElement tkTitle;
	
	@AndroidFindBy(id = "de.check24.check24:id/vk")
	public static AndroidElement vkTitle;
	
	@AndroidFindBy(id = "de.check24.check24:id/kh_price")
	public static AndroidElement ksPrice;
	
	@AndroidFindBy(id = "de.check24.check24:id/tk_price")
	public static AndroidElement tkPrice;
	
	@AndroidFindBy(id = "de.check24.check24:id/vk_price")
	public static AndroidElement vkPrice;
	
}
