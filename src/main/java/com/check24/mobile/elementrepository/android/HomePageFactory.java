package com.check24.mobile.elementrepository.android;

import com.check24.mobile.base.BaseMobilePage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class HomePageFactory extends BaseMobilePage {

	public HomePageFactory(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
	
	@AndroidFindBy(id = "de.check24.check24:id/layoutButtons")
	public static AndroidElement termAndCondition;
	
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ImageView")
	public static AndroidElement header;
	
	@AndroidFindBy(id = "de.check24.check24:id/homeSearchLayoutHintTextView")
	public static AndroidElement homeSearch;
	
	@AndroidFindBy(id = "de.check24.check24:id/search_src_text")
	public static AndroidElement searchTextField;
	
}
