package com.check24.mobile.pagehelpermethods.android;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

import com.check24.mobile.elementrepository.android.HomePageFactory;
import com.check24.utils.Util;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class HomePage extends HomePageFactory {

	public HomePage(AppiumDriver<MobileElement> driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
	
	public boolean isTermAndConditionDisplayed() {
		return isElementPresent(termAndCondition);
	}
	
	public void acceptTermAndCondition() {
		clickElement(termAndCondition);
	}
	
	public boolean isHomePageLoad() {
		return isElementPresent(header);
	}
	
	public void searchText(String searchText) {
		clickElement(homeSearch);
		sendKeys(searchTextField, searchText, true);
		keyTouch(Keys.ENTER);
		Util.sleepInSec(Util.L_WAIT, "Waiting for search to be performed");
	}
	
}
