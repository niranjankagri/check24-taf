package com.check24.mobile.pagehelpermethods.android;

import org.openqa.selenium.support.PageFactory;

import com.check24.mobile.elementrepository.android.KfzVersicherungPageFactory;
import com.check24.utils.Util;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class KfzVersicherungPage extends KfzVersicherungPageFactory {

	public KfzVersicherungPage(AppiumDriver<MobileElement> driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
	
	public boolean isSchnellpreisDisplayed() {
		return isElementPresent(schnellpreis);
	}
	
	public void clickOnSchnellpreis() {
		scrollDown();
		clickElement(schnellpreis);
	}
	
	public boolean isZipFieldDisplayed() {
		return isElementPresent(zip);
	}
	
	public boolean isZipHintDisplayed() {
		return isElementPresent(zipHint);
	}
	
	public void enterZip(String zipcode) {
		sendKeys(zip, zipcode, true);
		Util.sleepInSec(Util.S_WAIT, "Waiting for zip to indentify");
		isZipHintDisplayed();
	}
	
	public void enterZipcode(String zipcode) {
		enterZip(zipcode);
		clickOnWeiterButton();
	}
	
	public boolean isWeiterButtonDisplayed() {
		return isElementPresent(weiter);
	}
	
	public void clickOnWeiterButton() {
		isWeiterButtonDisplayed();
		clickElement(weiter);
	}
	
	public boolean isDobDayFieldsDisplayed() {
		return isElementPresent(dobDay);
	}
	
	public boolean isDobMonthFieldsDisplayed() {
		return isElementPresent(dobMonth);
	}
	
	public boolean isDobYearFieldsDisplayed() {
		return isElementPresent(dobYear);
	}
	
	public boolean isDobFieldsDisplayed() {
		return isDobDayFieldsDisplayed() && isDobMonthFieldsDisplayed() && isDobYearFieldsDisplayed();
	}
	
	public void enterDOBDay(String day) {
		sendKeys(dobDay, day, true);
	}
	
	public void enterDOBMonth(String month) {
		sendKeys(dobMonth, month, true);
	}
	
	public void enterDOBYear(String year) {
		sendKeys(dobYear, year, true);
	}
	
	public void enterDOB(String day, String month, String year) {
		enterDOBDay(day);
		enterDOBMonth(month);
		enterDOBYear(year);
		clickOnWeiterButton();
	}
	
	public boolean isDriveBarDisplayed() {
		return isElementPresent(driveBar);
	}
	
	public String getDriveValueDisplayed() {
		return getText(driveValue);
	}
	
	public void setDriveBar(String drive) {
		int startX = driveBar.getLocation().getX();
        int startY = driveBar.getLocation().getY();
        int endX = driveBar.getSize().getWidth();
        double increaseCount = 0.1;
        while(!getDriveValueDisplayed().equals(drive)) {
        	int moveToXDirectionAt = (int) (endX * increaseCount);
            raiseBar(startX, startY, moveToXDirectionAt);
            increaseCount = increaseCount + 0.05;
		}
        clickOnWeiterButton();
	}
	
	public boolean isHsnDisplayed() {
		return isElementPresent(hsnField);
	}
	
	public boolean isTsnDisplayed() {
		return isElementPresent(tsnField);
	}
	
	public void enterHSN(String hsn) {
		sendKeys(hsnField, hsn, true);
	}
	
	public void enterTSN(String tsn) {
		sendKeys(tsnField, tsn, true);
	}
	
	public void enterCarDetails(String hsn, String tsn) {
		enterHSN(hsn);
		enterTSN(tsn);
		Util.sleepInSec(Util.S_WAIT, "Waiting for car details to indentify");
		clickOnWeiterButton();
		Util.sleepInSec(Util.L_WAIT, "Waiting for car details to analyse");
	}
	
	public boolean isKsTitleDisplayed() {
		return isElementPresent(ksTitle);
	}
	
	public boolean isTkTitleDisplayed() {
		return isElementPresent(tkTitle);
	}
	
	public boolean isVkTitleDisplayed() {
		return isElementPresent(vkTitle);
	}
	
	public boolean isKsPriceDisplayed() {
		return isElementPresent(ksPrice);
	}
	
	public boolean isTkPriceDisplayed() {
		return isElementPresent(tkPrice);
	}
	
	public boolean isVkPriceDisplayed() {
		return isElementPresent(vkPrice);
	}
}
