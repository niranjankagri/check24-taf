package com.check24.mobile.appium.core;

import java.io.File;
import java.net.URL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.check24.mobile.base.MobileConstants;
import com.check24.utils.Util;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class AppiumDriverFactory {

	private static Logger logger = LogManager.getLogger(AppiumDriverFactory.class.getName());
	protected static AppiumDriver<MobileElement> driver;

	/**
	 * Get Android Driver
	 * 
	 * @return AndroidDriver
	 * @throws Exception
	 */
	public static AppiumDriver<MobileElement> getAndroidInstance() throws Exception {
		File fs = new File(System.getProperty("user.dir") + MobileConstants.APK_PATH);

		// Capabilities for Android to help set properties for the driver.
		DesiredCapabilities caps = new DesiredCapabilities();

		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "11.0");
		caps.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
//		caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12");
//		caps.setCapability(MobileCapabilityType.DEVICE_NAME, "9635956710000BL");
		caps.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
		caps.setCapability("unicodeKeyboard", true);
		caps.setCapability("resetKeyboard", true);

		caps.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
		driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		
		logger.info("Android application is launched");
		Util.sleepInSec(Util.L_WAIT, "Waiting for application to load");
		
		return driver;
	}
	
}
