package com.check24.mobile.appium.core;

import org.openqa.selenium.Dimension;

import com.check24.web.selenium.core.SeleniumDriverActions;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.FindsByAndroidUIAutomator;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class AppiumDriverActions extends SeleniumDriverActions {

	protected AppiumDriver<MobileElement> driver;
	protected TouchAction action;

	public AppiumDriverActions(AppiumDriver<MobileElement> driver) {
		super(driver);
		this.driver = driver;
		action = new TouchAction((PerformsTouchActions) driver);
	}

	/**
	 * Scroll down by text
	 * 
	 * @param element
	 */
	public void scrollDownByText(String visibleText) {
		((FindsByAndroidUIAutomator<MobileElement>) driver).findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""
						+ visibleText + "\").instance(0))");
	}

	/**
	 * Scroll down by text
	 * 
	 * @param element
	 */
	public void raiseBar(int startX, int startY, int moveToXDirectionAt) {
		action.press(PointOption.point(startX, startY)).moveTo(PointOption.point(moveToXDirectionAt, startY)).release()
				.perform();
	}

	public void scrollDown() {
		int pressX = driver.manage().window().getSize().width / 2;
		int bottomY = driver.manage().window().getSize().height * 4 / 5;
		int topY = driver.manage().window().getSize().height / 8;
		scroll(pressX, bottomY, pressX, topY);
	}

	public void scroll(int fromX, int fromY, int toX, int toY) {
		action.longPress(PointOption.point(fromX, fromY)).moveTo(PointOption.point(toX, toY)).release().perform();
	}

	public void scrollUp() {
		Dimension size = driver.manage().window().getSize();
		int starty = (int) (size.height * 0.20);
		int endy = (int) (size.height * 0.80);
		int startx = size.width / 2;
		scroll(startx, starty, startx, endy);
	}
}
