package com.check24.mobile.controller;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class PlatformController {

	AppiumDriver<MobileElement> driver;

	public PlatformController(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
	}

	public Platform getPlatform(String platform) {
		if (platform.equalsIgnoreCase("android")) {
			return new Android(driver);
		}
		return null;
	}

}
