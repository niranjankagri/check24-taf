package com.check24.mobile.controller;

import com.check24.mobile.pagehelpermethods.android.HomePage;
import com.check24.mobile.pagehelpermethods.android.KfzVersicherungPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class Android implements Platform {

	AppiumDriver<MobileElement> driver;

	public Android(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
	}

	@Override
	public HomePage getHomePage() {
		return new HomePage(driver);
	}

	@Override
	public KfzVersicherungPage getKfzVersicherungPage() {
		return new KfzVersicherungPage(driver);
	}
	
}
