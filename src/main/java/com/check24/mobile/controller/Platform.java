package com.check24.mobile.controller;

public interface Platform {

	abstract public <T> T getHomePage();
	abstract public <T> T getKfzVersicherungPage();
	
}
