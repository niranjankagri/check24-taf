package com.check24.mobile.base;

import org.openqa.selenium.By;

import com.check24.mobile.appium.core.AppiumDriverActions;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class BaseMobilePage extends AppiumDriverActions {

	public BaseMobilePage(AppiumDriver<MobileElement> driver) {
		super(driver);
	}
	
	public boolean isTextDisplayed(String visibleText) {
//		scrollDownByText(visibleText);
		return isDisplayed(driver.findElement(By.xpath("//android.widget.TextView[@text='" + visibleText + "']")));
	}
	
	public void clickViaText(String visibleText) {
//		scrollDownByText(visibleText);
		clickElement(driver.findElement(By.xpath("//*[@text='" + visibleText + "']")));
	}

}
