 package com.check24.mobile.base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.check24.mobile.appium.core.AppiumDriverFactory;
import com.check24.mobile.controller.Platform;
import com.check24.mobile.controller.PlatformController;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class BaseMobileTestSuite {

	protected static AppiumDriver<MobileElement> driver;
    protected Logger logger = LogManager.getLogger(BaseMobileTestSuite.class.getName());
    public static Platform app;
    
	/**
	 * Create an instance of webdriver and initiate all page
	 * 
	 * @param platform
	 * @param locale
	 * @throws Exception
	 */
	@Parameters({"platform"})
	@BeforeClass (alwaysRun = true)
	public void setUp(String platform) throws Exception{
		
	    if (platform.equalsIgnoreCase("android")) {
			driver = AppiumDriverFactory.getAndroidInstance();
			logger.info("APPIUMDRIVER IS STARTING ON LOCAL SESSION");
		}
	    
	    PlatformController controller = new PlatformController(driver);
	    app = controller.getPlatform(platform);
	}

    /**
	 * Closes the current Session
	 */
	protected void quitSession() {
		driver.close();
	}
	
	/**
	 * Quit the driver, once the tests are completed
	 */
	@AfterClass	(alwaysRun = true)
	public void tearDown() {
		try {
			if (driver != null) 
			driver.quit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    public static AppiumDriver<MobileElement> getDriver() {
        return driver;
    }
	
}
