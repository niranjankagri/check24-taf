package com.check24.mobile.base;

public class MobileConstants {

	public static final String APK_PATH = "/resources/app/CHECK24.apk";
	public static final String SEARCH_TEXT = "KFZ-Versicherung";
	public static final String ZIPCODE = "56072";
	public static final String DOB_DAY = "27";
	public static final String DOB_MONTH = "03";
	public static final String DOB_YEAR = "1990";
	public static final String DRIVE = "11.000";
	public static final String WHO_DRIVE_CAR_PAGE_TITLE = "Wer fährt mit dem Auto?";
	public static final String CAR_INSURED_PAGE_TITLE = "Haben Sie schon einmal selbst ein Auto versichert?";
	public static final String PAY_SCHEDULE_PAGE_TITLE = "Wie möchten Sie bezahlen?";
	public static final String PAY_YEARLY = "jährlich";
	public static final String HSN = "0588";
	public static final String TSN = "588";
	public static final String PROTECTION_PAGE_TITLE = "Herzlichen Glückwunsch, wählen Sie Ihren passenden Schutz!";
	
}
