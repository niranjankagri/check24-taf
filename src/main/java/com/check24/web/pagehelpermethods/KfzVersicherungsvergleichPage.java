package com.check24.web.pagehelpermethods;

import org.openqa.selenium.WebDriver;

import com.check24.utils.Util;
import com.check24.web.elementrepository.KfzVersicherungsvergleichPageFactory;

public class KfzVersicherungsvergleichPage extends KfzVersicherungsvergleichPageFactory {

	public KfzVersicherungsvergleichPage(WebDriver driver) {
        super(driver);
    }
	
	public boolean isAendernButtonDisplayed() {
		return isElementPresent(aendern);
	}
	
	public void clickOnAendernButton() {
		clickElement(aendern);
	}
	
	public boolean isWeiterButtonDisplayed() {
		return isElementPresent(weiter);
	}
	
	public void clickOnWeiterButton() {
		clickElement(weiter);
	}
	
	public boolean isVersicherungUploadDisplayed() {
		return isElementPresent(versicherungUpload);
	}
	
	public void uploadVersicherung(String filePath) {
		executeScript("arguments[0].setAttribute('style', 'display: block;')", versicherungUpload);
		sendKeys(versicherungUpload, System.getProperty("user.dir") + filePath, true);
		executeScript("arguments[0].setAttribute('style', 'display: none;')", versicherungUpload);
	}
	
	public boolean isHochladenAndAnalysierenButtonDisplayed() {
		return isElementPresent(hochladenAndAnalysieren);
	}
	
	public void clickOnHochladenAndAnalysierenButton() {
		clickElement(hochladenAndAnalysieren);
		Util.sleepInSec(Util.L_WAIT, "Waiting for file to upload and analyze");
	}
	
	public boolean isNeinMitVorhandenemDokumentWeiterButtonDisplayed() {
		return isElementPresent(neinMitVorhandenemDokumentWeiter);
	}
	
	public void clickOnNeinMitVorhandenemDokumentWeiterButton() {
		clickElement(neinMitVorhandenemDokumentWeiter);
	}
	
	public boolean isOhneAnmeldungFortfahrenButtonDisplayed() {
		return isElementPresent(ohneAnmeldungFortfahren);
	}
	
	public void clickOnOhneAnmeldungFortfahrenButton() {
		clickElement(ohneAnmeldungFortfahren);
	}
	
	public boolean isFahrzeugSectionDisplayed() {
		return isElementPresent(fahrzeug);
	}
	
	public boolean isFahrzeugauswahlüberLabelDisplayed() {
		return isElementPresent(fahrzeugauswahlüber);
	}
	
	public boolean isTypschlüsselnummerLabelDisplayed() {
		return isElementPresent(typschlüsselnummer);
	}
	
	public boolean isHerstellernummerLabelDisplayed() {
		return isElementPresent(herstellernummer);
	}
	
	public boolean isVersicherungsbeginnLabelDisplayed() {
		return isElementPresent(versicherungsbeginn);
	}
	
}
