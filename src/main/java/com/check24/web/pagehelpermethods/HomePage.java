package com.check24.web.pagehelpermethods;

import org.openqa.selenium.WebDriver;

import com.check24.utils.Util;
import com.check24.web.elementrepository.HomePageFactory;

public class HomePage extends HomePageFactory {

	public HomePage(WebDriver driver) {
        super(driver);
    }
	
	public void clickOnBestehendesAuto() {
		clickElement(bestehendesAuto);
	}
	
	public void clickOnTarifeVergleichen() {
		clickElement(tarifeVergleichen);
		Util.sleepInSec(Util.S_WAIT, "waiting for insurance to process");
	}
	
	public void clickOnAkzeptieren() {
		clickElement(akzeptieren);
	}
	
	public boolean isHomePageLoad() {
		return isElementPresent(akzeptieren);
	}
	
	public boolean isBestehendesAutoSelectionDisplayed() {
		return isElementPresent(bestehendesAuto);
	}
	
}
