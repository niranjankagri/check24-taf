package com.check24.web.selenium.core;

import java.io.File;
import java.time.Duration;
import java.util.NoSuchElementException;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import com.check24.utils.Util;

/**
 * This class has wrapper functions for any Browser actions that can be taken on
 * a web page like click, sendKeys, openUrl, etc,.
 */
public class SeleniumDriverActions {

	protected WebDriver driver;
	protected Wait<WebDriver> webDriverWait;
	public Logger logger = LogManager.getLogger();
	public static final long DEFAULT_WAIT_TIME = 30;
	public static final long POOLING_WAIT_TIME = 2;
	protected Actions actions;

	public SeleniumDriverActions(WebDriver webDriver) {
		this.driver = webDriver;
		actions = new Actions(webDriver);
		this.webDriverWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(DEFAULT_WAIT_TIME))
				.pollingEvery(Duration.ofSeconds(POOLING_WAIT_TIME)).ignoring(NoSuchElementException.class);
	}

	/**
	 * Click on a WebElement
	 * 
	 * @param WebElement
	 */
	public boolean clickElement(WebElement element) {
		boolean result = false;
		if (element != null) {

			element.click();
			logger.info("Clicked On the element: " + element);
			result = true;

		}
		return result;
	}

	/**
	 * Click on a WebElement using JavaScriptExecutor
	 * 
	 * @param WebElement
	 */
	public void clickJavaScript(WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
		logger.info("Clicked on the WebElement" + element);
	}


	/**
	 * Wrapper function to enter text in a webElement (aka sendKeys), clearing
	 * existing data if needed
	 * 
	 * @param element           - WebElement to perform Click operation
	 * @param data              - data to be sent
	 * @param clearExistingData - true/false
	 * @return
	 */
	public void sendKeys(WebElement element, String data, Boolean clearExistingData) {
		Util.sleepInSec(Util.S_WAIT);
		this.webDriverWait.until(ExpectedConditions.visibilityOf(element));
		try {
			if (clearExistingData) {
				element.clear();
			}
			element.sendKeys(data);
			logger.info("Send Keys On Element :: " + element + " With Data : " + data);
		} catch (Exception e) {
			logger.error("Cannot enter data on element :" + element);
			logger.info("Stacktrace: \n", e);
		}
	}

	/**
	 * Execute javascript
	 * 
	 * @param script
	 * @param args
	 */
	public void executeScript(String script, Object... args) {
		((JavascriptExecutor) driver).executeScript(script, args);
	}

	/**
	 * Verify if the element is present
	 * 
	 * @param element
	 * @return
	 */
	public boolean isElementPresent(WebElement element) {
		if (element != null) {
			logger.info("Element : " + element + " successfully appeared on page");
			return true;
		} else {
			logger.warn("Element : " + element + " is not present on page");
			return false;
		}
	}

	/**
	 * Take Screenshot
	 * 
	 * @param issueType
	 */
	public void screenShot(String issueType) {
		String fileName = System.currentTimeMillis() + "." + issueType + ".png";
		File screenshotDir = new File("resources/screencapture");
		screenshotDir.mkdir();
		if (fileName.contains("/")) {
			fileName = fileName.replaceAll("/", "-");
			logger.info(fileName);
		}
		try {
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshot, new File(screenshotDir + "//" + fileName));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * sendKeys
	 *
	 * @param key - example KEYS.CONTROL, KEYS.DELETE
	 */
	public void keyTouch(Keys key) {
		Actions action = new Actions(driver);
		action.sendKeys(key);
		action.perform();
	}
	
	/**
	 * Navigate back
	 */
	public void navigateBack() {
		Util.sleepInSec(Util.M_WAIT, "Wait before navigating back");
		driver.navigate().back();
		Util.sleepInSec(Util.M_WAIT, "Wait for navigating back");
	}
	
	/**
	 * Waits for element to be clickable using WebElement
	 * 
	 * @param element
	 */
	public void isElementToBeClickAble(WebElement element) {
		this.webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
	}

	/**
	 * 
	 * Gets the text from the WebElement passed
	 * 
	 * @param element
	 * @return
	 */
	public String getText(WebElement element) {
		String text = null;
		if (element != null) {
			text = element.getText();
			if (text.length() != 0)
				logger.info("Getting Text On Element :: " + element + " The Text is : " + text);
		} else {
			logger.error("");
			return null;
		}
		return text.trim();
	}
	
	/**
	 * Returns the state of the element, if the element is displayed or not, based on WebElement
	 * 
	 * @param element
	 * @return
	 */

	public boolean isDisplayed(WebElement element) {
		return (element).isDisplayed() ? true : false;
	}
	
	/**
	 * Waits for element to be visible using WebElement
	 * 
	 * @param element
	 */
	public void isElementToBeVisible(WebElement element) {
		this.webDriverWait.until(ExpectedConditions.visibilityOfAllElements(element));
	}
}
