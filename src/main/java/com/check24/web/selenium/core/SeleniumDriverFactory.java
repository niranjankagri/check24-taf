package com.check24.web.selenium.core;

import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import com.check24.utils.Util;
import com.check24.web.base.WebConstants;
import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumDriverFactory {
	
	private static Logger  logger = LogManager.getLogger(SeleniumDriverFactory.class.getName());
	
    public static WebDriver getChromeDriver() {
    	WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-fullscreen");
        return new ChromeDriver(chromeOptions);
    }
    
    public static WebDriver getFirefoxDriver() {
    	WebDriverManager.firefoxdriver().setup();
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setLegacy(false);
        return new FirefoxDriver(firefoxOptions);
    }

    /**
     * Get instance of WebDriver interface
     * @param browser
     * @return
     */
    public static WebDriver getLocalInstance(String browser) throws Exception {
        WebDriver driver = null;
        
        if (browser.equalsIgnoreCase("Firefox")) {
            driver = getFirefoxDriver();
        } else if (browser.equalsIgnoreCase("Chrome")) {
            driver = getChromeDriver();
        }
        
        driver.manage().window().maximize();
        
        // Setting Driver Implicit Time out for An Element
        driver.manage().timeouts().implicitlyWait(Util.M_WAIT, TimeUnit.SECONDS);

        // loading Browser with App URL
        driver.get(WebConstants.BASE_URL);
        logger.info("The URL is " + WebConstants.BASE_URL);
        
        return driver;
    }

}
