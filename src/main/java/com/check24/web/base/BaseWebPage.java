package com.check24.web.base;

import org.openqa.selenium.WebDriver;

import com.check24.web.selenium.core.SeleniumDriverActions;

public class BaseWebPage extends SeleniumDriverActions {

	public BaseWebPage(WebDriver webDriver) {
		super(webDriver);
	}

}
