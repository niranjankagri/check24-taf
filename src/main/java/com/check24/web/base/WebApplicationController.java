package com.check24.web.base;

import org.openqa.selenium.support.PageFactory;

import com.check24.web.pagehelpermethods.HomePage;
import com.check24.web.pagehelpermethods.KfzVersicherungsvergleichPage;

public class WebApplicationController {
	
	protected HomePage homePage;
	protected KfzVersicherungsvergleichPage kfzVersicherungsvergleichPage;
	
	public HomePage getHomePage() {
        if(homePage == null) {
        	homePage = PageFactory.initElements(BaseWebTestSuite.getDriver(), HomePage.class);
        }
        return homePage;
    }
	
	public KfzVersicherungsvergleichPage getKfzVersicherungsvergleichPage() {
        if(kfzVersicherungsvergleichPage == null) {
        	kfzVersicherungsvergleichPage = PageFactory.initElements(BaseWebTestSuite.getDriver(), KfzVersicherungsvergleichPage.class);
        }
        return kfzVersicherungsvergleichPage;
    }

}
