package com.check24.web.base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.check24.web.selenium.core.SeleniumDriverFactory;

/**
 * This function defines the core selenium actions,
 * 
 * @BeforeClass - Before every class, creating a new webDriver instance
 * @AfterClass - Quit the WebDriver when the test is completed.
 * 
 *             All the respective webPage related classes (which contain the
 *             functions specific to that particular webPage) are also initiated
 *             here, in the setUp function
 * 
 *             This class needs to be extended, for every new automation
 *             testClass
 *
 */
public class BaseWebTestSuite extends WebApplicationController {

	protected static WebDriver driver;
	protected Logger logger = LogManager.getLogger(BaseWebTestSuite.class.getName());

	/**
	 * Create an instance of webdriver and initiate all page
	 * 
	 * @param browser
	 */
	@Parameters({ "browser" })
	@BeforeClass(alwaysRun = true)
	public void setUp(String browser) throws Exception {
		driver = SeleniumDriverFactory.getLocalInstance(browser);
		logger.info("WEBDRIVER IS STARTING ON LOCAL SESSION");
	}

	/**
	 * Closes the current Session
	 */
	protected void quitSession() {
		driver.close();
	}

	/**
	 * Quit the driver, once the tests are completed
	 */
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		try {
			if (driver != null)
				driver.quit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static WebDriver getDriver() {
		return driver;
	}

}
