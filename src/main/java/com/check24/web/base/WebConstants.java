package com.check24.web.base;

public class WebConstants {

	public static final String BASE_URL = "https://www.check24.de/kfz-versicherung";
	public static final String VERSICHERUNG_FILE_PATH = "\\resources\\testfiles\\Test Automation Task.pdf";
	
}
