package com.check24.web.elementrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.check24.web.base.BaseWebPage;

public class KfzVersicherungsvergleichPageFactory extends BaseWebPage {

	public KfzVersicherungsvergleichPageFactory(WebDriver driver) {
        super(driver);
    }

    @FindBy(id ="c24_situation/SITUATION_SECTION_aendern")
    public WebElement aendern;
    
    @FindBy(xpath ="//button[text()='weiter']")
    public WebElement weiter;
    
    @FindBy(id ="c24_ftr_capture")
    public WebElement versicherungUpload;
    
    @FindBy(xpath ="//button[text()='jetzt hochladen und analysieren']")
    public WebElement hochladenAndAnalysieren;
    
    @FindBy(xpath ="//button[text()='nein, mit vorhandenem Dokument weiter']")
    public WebElement neinMitVorhandenemDokumentWeiter;
    
    @FindBy(xpath ="//button[text()='ohne Anmeldung fortfahren']")
    public WebElement ohneAnmeldungFortfahren;
    
    @FindBy(id ="c24_opbsection_vehicle/VEHICLE_SECTION")
    public WebElement fahrzeug;
    
    @FindBy(xpath ="//div[text()='Fahrzeugauswahl über']")
    public WebElement fahrzeugauswahlüber;
    
    @FindBy(xpath ="//div[text()='Herstellernummer']")
    public WebElement herstellernummer;
    
    @FindBy(xpath ="//div[text()='Typschlüsselnummer']")
    public WebElement typschlüsselnummer;
    
    @FindBy(xpath ="//div[text()='Versicherungsbeginn']")
    public WebElement versicherungsbeginn;
	
}
