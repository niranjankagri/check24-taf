package com.check24.web.elementrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.check24.web.base.BaseWebPage;

public class HomePageFactory extends BaseWebPage {

	public HomePageFactory(WebDriver driver) {
        super(driver);
    }

    @FindBy(id ="wechseln_pkw")
    public WebElement bestehendesAuto;
    
    @FindBy(xpath ="//button[text()='Tarife vergleichen']")
    public WebElement tarifeVergleichen;
    
    @FindBy(linkText ="Akzeptieren")
    public WebElement akzeptieren;
    
}
